(* whenjobs daemon
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

let tutorial = "\
(* Everything in (* ... *) is a comment.
   Everything in << ... >> is a shell script.  Use >\\> to escape >> in script.

   This is just a quick start.  For full documentation read the
   whenjobs(1) man page by typing 'man 1 whenjobs'

   Use 'every <period> : << >>' to run a shell script periodically
   (like cron).  Don't forget the colon!

every 10 minutes :
<<
  # Get free blocks in /home
  free=`stat -f -c %b /home`
  # Set the variable 'free_space'
  whenjobs --set --type int free_space=$free
>>

   Use 'when <expr> : << >>' runs the shell script only when
   the expression is true.  'changes' is a function that monitors
   a variable and returns true when it changes value.

when changes free_space && free_space < 100000 :
<<
  mail -s \"ALERT: only $free_space blocks left on /home\" $LOGNAME </dev/null
>>

*)
"
