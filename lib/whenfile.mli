(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** This module is used when compiling whenjobs input files. *)

val init : Whenstate.t -> unit
(** "Initialize" the module.  Pass in the initial state, ready for
    parsing a new file. *)

val get_state : unit -> Whenstate.t
(** Return the updated state.  Call this after parsing the file. *)

val add_when_job : Camlp4.PreCast.Loc.t -> string -> Whenexpr.pre option -> Whenexpr.post option -> Camlp4.PreCast.Ast.expr -> Whenexpr.shell_script -> unit
(** When a 'when' macro appears as a toplevel statement in an
    input file, it causes this function to be called.

    [loc] is the location in the input file.

    [name] is the name of the job.

    [pre] and [post] are the optional pre and post functions.

    [expr] is the expression, as an OCaml abstract syntax tree.

    [sh] is the shell script fragment (basically location + a big string). *)

val add_every_job : Camlp4.PreCast.Loc.t -> string -> Whenexpr.pre option -> Whenexpr.post option -> Whenexpr.periodexpr -> Whenexpr.shell_script -> unit
(** When an 'every' macro appears as a toplevel statement in an
    input file, it causes this function to be called.

    [loc] is the location in the input file.

    [name] is the name of the job.

    [pre] and [post] are the optional pre and post functions.

    [periodexpr] is the period, eg. 30 seconds.

    [sh] is the shell script fragment. *)

val set_variable : string -> Whenexpr.variable -> unit
(** Set a variable during file load. *)
