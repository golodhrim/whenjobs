(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** Handle the locking that ensures only one instance of the
    daemon can run per user. *)

val create_lock : string -> unit
(** [create_lock jobsdir] creates the lock file (called [daemon_pid])
    in the [jobsdir] directory.  The lock is automatically released
    when the process exits. *)

val update_pid : unit -> unit
(** Call this if the PID of the program changes, ie. after fork. *)

val test_locked : string -> bool
(** Test if there is a lock file and the lock is held by another process. *)

val kill_daemon : string -> unit
(** If there is a daemon holding the lock, kill it. *)
