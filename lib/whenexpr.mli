(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** When- and every-expression definition and evaluation, variables
    and jobs. *)

type whenexpr =
  | Expr_unit                           (** Unit constant. *)
  | Expr_bool of bool                   (** A boolean constant. *)
  | Expr_str of string                  (** A string constant. *)
  | Expr_int of Big_int.big_int         (** An integer constant. *)
  | Expr_float of float                 (** A float constant. *)
  | Expr_var of string                  (** A variable name. *)
  | Expr_and of whenexpr * whenexpr     (** && *)
  | Expr_or of whenexpr * whenexpr      (** || *)
  | Expr_lt of whenexpr * whenexpr      (** < *)
  | Expr_le of whenexpr * whenexpr      (** <= *)
  | Expr_eq of whenexpr * whenexpr      (** == *)
  | Expr_ge of whenexpr * whenexpr      (** >= *)
  | Expr_gt of whenexpr * whenexpr      (** > *)
  | Expr_ne of whenexpr * whenexpr      (** != *)
  | Expr_not of whenexpr                (** boolean not *)
  | Expr_add of whenexpr * whenexpr     (** arithmetic addition or string cat *)
  | Expr_sub of whenexpr * whenexpr     (** arithmetic subtraction *)
  | Expr_mul of whenexpr * whenexpr     (** arithmetic multiplication *)
  | Expr_div of whenexpr * whenexpr     (** arithmetic division *)
  | Expr_mod of whenexpr * whenexpr     (** arithmetic modulo *)
  | Expr_len of whenexpr                (** length *)
  | Expr_changes of string              (** changes var *)
  | Expr_increases of string            (** increases var *)
  | Expr_decreases of string            (** decreases var *)
  | Expr_prev of string                 (** prev var *)
  | Expr_reloaded                       (** reloaded () *)
(** Internal type used to represent 'when' expressions. *)

type periodexpr =
  | Every_seconds of int
  | Every_days of int
  | Every_months of int
  | Every_years of int
(** Internal type used to represent 'every' expressions. *)

type shell_script = {
  sh_loc : Camlp4.PreCast.Loc.t;
  sh_script : string;
}
(** A shell script. *)

type variable =
  | T_unit
  | T_bool of bool
  | T_string of string
  | T_int of Big_int.big_int
  | T_float of float
(** Typed variable (see also [whenproto.x]) *)

val string_of_variable : variable -> string

val variable_of_rpc : Whenproto_aux.variable -> variable
val rpc_of_variable : variable -> Whenproto_aux.variable

type variables = variable Whenutils.StringMap.t
(** A set of variables. *)

type preinfo = {
  pi_job_name : string;                 (** Job name. *)
  pi_serial : Big_int.big_int;          (** Job serial number. *)
  pi_variables : (string * variable) list; (** Variables set in job. *)
  pi_running : preinfo_running_job list; (** List of running jobs. *)
}
and preinfo_running_job = {
  pirun_job_name : string;              (** Running job name. *)
  pirun_serial : Big_int.big_int;       (** Running job serial number. *)
  pirun_start_time : float;             (** Running job start time. *)
  pirun_pid : int;                      (** Running job process ID. *)
}
(** Information available to pre function before the job runs. *)

type result = {
  res_job_name : string;                (** Job name. *)
  res_serial : Big_int.big_int;         (** Job serial number. *)
  res_code : int;                       (** Return code from the script. *)
  res_tmpdir : string;                  (** Temporary directory. *)
  res_output : string;                  (** Filename of output from job. *)
  res_start_time : float;               (** When the job started. *)
}
(** Result of the run of a job. *)

type pre = preinfo -> bool
type post = result -> unit
(** Pre and post functions. *)

type job_cond =
  | When_job of whenexpr                (** when ... : << >> *)
  | Every_job of periodexpr             (** every ... : << >> *)

type job = {
  job_loc : Camlp4.PreCast.Loc.t;
  job_name : string;
  job_pre : pre option;
  job_post : post option;
  job_cond : job_cond;
  job_script : shell_script;
}
(** A job. *)

val expr_of_ast : Camlp4.PreCast.Ast.Loc.t -> Camlp4.PreCast.Ast.expr -> whenexpr
(** Convert OCaml AST to an expression.  Since OCaml ASTs are much
    more general than the expressions we can use, this can raise
    [Invalid_argument] in many different situations. *)

val string_of_whenexpr : whenexpr -> string
(** Pretty-print an expression to a string. *)

val string_of_periodexpr : periodexpr -> string
(** Pretty-print a period expression to a string. *)

val dependencies_of_whenexpr : whenexpr -> string list
(** Return list of variables that an expression depends on.  This is
    used to work out when an expression needs to be reevaluated. *)

val dependencies_of_job : job -> string list
(** Which variables does this job depend on? *)

val eval_whenexpr : variables -> variables option -> bool -> whenexpr -> variable
val eval_whenexpr_as_bool : variables -> variables option -> bool -> whenexpr -> bool
(** [eval_whenexpr variables prev_variables onload expr] is the
    evaluation function for when expressions.  It full evaluates
    [expr], returning its typed value.  It can also throw exceptions
    (at least [Invalid_argument] and [Failure]).

    [eval_whenexpr_as_bool] is the same but it forces the returned
    value to be a boolean.

    The other parameters represent the current and past state:

    [variables] is the current set of variables and their values.

    [prev_variables] is the set of variables from the previous
    run.  It is used to implement {i prev}, {i changes} etc operators.
    This can be [None], meaning there is no previous state.

    [onload] is used to implement the {i reloaded} operator.  It is
    true if the file is being reloaded, and false otherwise. *)

val next_periodexpr : float -> periodexpr -> float
(** [next_periodexpr t period] returns the earliest event of [period]
    strictly after time [t].

    Visualising periods as repeated events on a timeline, this
    returns [t']:

    {v
    events:  ---+---------+---------+---------+---------+---------+-----
    times:          t     t'
    }

    Note that [periodexpr] events are not necessarily regular.
    eg. The start of a month is not a fixed number of seconds
    after the start of the previous month.  'Epoch' refers
    to the Unix Epoch (ie. 1970-01-01 00:00:00 UTC).

    If [period = Every_seconds i] then events are when
    [t' mod i == 0] when t' is the number of seconds since
    the Epoch.  This returns the next t' > t.

    If [period = Every_days i] then events happen at
    midnight UTC every [i] days since the Epoch.
    This returns the next midnight > t.

    If [period = Every_months i] then events happen at
    midnight UTC on the 1st day of the month every [i] months
    since the Epoch.  This returns midnight on the
    1st day of the next month > t.

    If [period = Every_years i] then events happen at
    midnight UTC on the 1st day of the year when
    [(y - 1970) mod i == 0].  This returns midnight on the
    1st day of the next year > t. *)

val check_valid_variable_name : string -> unit
(** Check that [name] is a valid variable name that users are permitted
    to set, and raise [Failure] if it is not.  *)
