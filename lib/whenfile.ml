(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

open Whenexpr

open Printf

(* The state updated during parsing of the file. *)
let state = ref Whenstate.empty

let init s = state := s

let add_when_job _loc name pre post e sh =
  let e = expr_of_ast _loc e in
  let job = { job_loc = _loc; job_name = name;
              job_pre = pre; job_post = post;
              job_cond = When_job e; job_script = sh } in
  state := Whenstate.add_job !state job

let add_every_job _loc name pre post e sh =
  let job = { job_loc = _loc; job_name = name;
              job_pre = pre; job_post = post;
              job_cond = Every_job e; job_script = sh } in
  state := Whenstate.add_job !state job

let set_variable name value =
  state := Whenstate.set_variable !state name value

let get_state () = !state
