(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** [Whentools] contains helper functions that jobs can call as they
    are loading. *)

val set_variable : string -> string -> unit
(** Set variable (just a wrapper around {!Whenfile.set_variable}). *)

val set_variable_bool : string -> bool -> unit
(** Set variable (just a wrapper around {!Whenfile.set_variable}). *)

val set_variable_int : string -> int -> unit
(** Set variable (just a wrapper around {!Whenfile.set_variable}). *)

val set_variable_string : string -> string -> unit
(** Set variable (just a wrapper around {!Whenfile.set_variable}). *)

val set_variable_float : string -> float -> unit
(** Set variable (just a wrapper around {!Whenfile.set_variable}). *)

type preinfo = Whenexpr.preinfo

val max : int -> preinfo -> bool
val one : unit -> preinfo -> bool
(** Pre functions to limit number of jobs running. *)

type result = Whenexpr.result

val mailto : ?only_on_failure:bool -> ?from:string -> string -> result -> unit
(** Post function to send mail. *)
