/* whenjobs -*- c -*-
 * (C) Copyright 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This is the protocol used to talk between the client (whenjobs) and
 * the daemon (whenjobsd).  Communication happens over a Unix domain
 * socket '$HOME/.whenjobs/socket'.  The wire protocol is SunRPC.
 */

/* Maximum lengths and some useful typedefs. */
const MAX_VARIABLE_NAME_LENGTH = 256;
const MAX_VARIABLE_VALUE_LENGTH = 65536;
const MAX_JOB_NAME_LENGTH = 256;
const MAX_BIG_INT_LENGTH = 64; /* when encoded as a string */
const MAX_PATH_LENGTH = 4096;

typedef string variable_name<MAX_VARIABLE_NAME_LENGTH>;
typedef string string_value<MAX_VARIABLE_VALUE_LENGTH>;
typedef string job_name<MAX_JOB_NAME_LENGTH>;
typedef string string_big_int<MAX_BIG_INT_LENGTH>;
typedef string path<MAX_PATH_LENGTH>;

typedef variable_name variable_name_list<>;

/* Status code (OK or error) returned by most calls. */
enum status_code {
  OK = 1,
  ERROR = 2
};

union status switch (status_code s) {
 case OK:
   void;
 case ERROR:
   string error<>;
};

enum variable_type {
  UNIT_T = 0,
  BOOL_T = 1,
  STRING_T = 2,
  INT_T = 3,
  FLOAT_T = 4
};

union variable switch (variable_type t) {
 case UNIT_T:
   void;
 case BOOL_T:
   bool b;
 case STRING_T:
   string_value s;
 case INT_T:
   string_big_int i;            /* OCaml [big_int], as a string. */
 case FLOAT_T:
   double f;                    /* C 'double' maps to an OCaml 'float' */
};

struct set_variable {
  variable_name sv_name;
  variable sv_value;
};
typedef set_variable set_variable_list<>;

struct job {
  job_name job_name;
  string_big_int job_serial;
  path job_tmpdir;
  hyper job_start_time;
};

typedef job job_list<>;

typedef job_name job_name_list<>;

/* The API of the daemon. */
program When {
  version V1 {
    status reload_file (void) = 1;
    status set_variable (variable_name, variable) = 2; /* obsolete */
    variable get_variable (variable_name) = 3;
    variable_name_list get_variable_names (void) = 4;
    status exit_daemon (void) = 5;
    job_list get_jobs (void) = 6;
    status cancel_job (string_big_int) = 7;
    status start_job (job_name) = 8;
    job get_job (string_big_int) = 9;
    status set_variables (set_variable_list) = 10;
    job_name_list get_job_names (void) = 11;
    job_name_list test_variables (set_variable_list) = 12;
    status ping_daemon (void) = 13;
    status whisper_variables (set_variable_list) = 14;
  } = 1;
} = 0x20008081;
