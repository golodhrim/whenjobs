(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

open Camlp4.PreCast
open Ast

open CalendarLib

open Big_int
open Unix
open Printf

module StringMap = struct
  include Map.Make (String)
  let keys m = fold (fun k _ ks -> k :: ks) m []
  let values m = fold (fun _ v vs -> v :: vs) m []
end

module IntMap = struct
  include Map.Make (struct type t = int let compare = compare end)
  let keys m = fold (fun k _ ks -> k :: ks) m []
  let values m = fold (fun _ v vs -> v :: vs) m []
end

module BigIntMap = struct
  include Map.Make (struct type t = big_int let compare = compare_big_int end)
  let keys m = fold (fun k _ ks -> k :: ks) m []
  let values m = fold (fun _ v vs -> v :: vs) m []
end

module StringSet = Set.Make (String)

let (//) = Filename.concat

let isalpha = function 'a'..'z' | 'A'..'Z' -> true | _ -> false
let isalnum = function 'a'..'z' | 'A'..'Z' | '0'..'9' -> true | _ -> false

let rec filter_map f = function
  | [] -> []
  | x :: xs ->
    match f x with
    | Some y -> y :: filter_map f xs
    | None -> filter_map f xs

let string_of_time_t ?(localtime = false) t =
  let tm = (if localtime then Unix.localtime else gmtime) t in
  sprintf "%04d-%02d-%02d %02d:%02d:%02d%s"
    (1900+tm.tm_year) (1+tm.tm_mon) tm.tm_mday
    tm.tm_hour tm.tm_min tm.tm_sec
    (if localtime then "" else " UTC")

let string_startswith str prefix =
  let len = String.length str in
  let plen = String.length prefix in
  len >= plen && String.sub str 0 plen = prefix

let string_endswith str suffix =
  let len = String.length str in
  let slen = String.length suffix in
  len >= slen && String.sub str (len-slen) slen = suffix
