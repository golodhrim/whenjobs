(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** The state of jobs and variables. *)

type t
(** This opaque, immutable type represents the state of jobs and
    variables from a loaded and running jobs file.

    You can create an empty state by calling {!empty}.  This state
    has no jobs and no variables.

    You can then add jobs and set variables by calling
    {!add_job} and {!set_variable}.  You can also copy variables
    from an old state to a new state (used when reloading the
    jobs file).

    The rest of the functions deal with querying the state
    and are mainly used by the daemon (see [Daemon] module). *)

val empty : t
(** Return an empty state.  This state has no jobs or variables. *)

val add_job : t -> Whenexpr.job -> t
(** Add a job to the state, returning a new state. *)

val set_variable : t -> string -> Whenexpr.variable -> t
(** Set/update the value of a variable, returning a new state. *)

val copy_variables : t -> t -> t
(** [copy_variables old_state current_state -> new_state] copies
    the variables from [old_state], adding them to [current_state],
    returning a new state.  Note the order of arguments. *)

val copy_prev_state : t -> t -> t
(** [copy_prev_state old_state current_state -> new_state] is an
    obscure function used to make the [prev] function work predictably
    across file reloads.  Since a file reload creates a new state
    object, it would normally "forget" that jobs had run previously,
    so any job that used [prev], [changes] etc would run again
    unnecessarily.  This function copies the prev state, allowing us
    to remember which jobs ran previously and the state of the
    variables at that time, making these functions work predictably.
    State is only copied for jobs that have explicit names. *)

val get_variable : t -> string -> Whenexpr.variable
(** Return the value of a variable, when unknown variables defaulting
    to empty string. *)

val get_variables : t -> (string * Whenexpr.variable) list
(** Return the value of all variables.  Variables that are empty
    strings are not returned. *)

val get_variable_names : t -> string list
(** Return all variable names.  Variables that are empty strings are
    ignored. *)

val nr_jobs : t -> int
(** Returns the number of jobs in the state. *)

val get_dependencies : t -> string list -> Whenexpr.job list
(** Return the jobs which depend on the named variables. *)

val get_whenjobs : t -> Whenexpr.job list
val get_everyjobs : t -> Whenexpr.job list
(** Return all of the when-jobs / every-jobs. *)

val get_job : t -> string -> Whenexpr.job
(** Return the named job, or raise [Not_found]. *)

val get_job_names : t -> string list
(** Return the names of all jobs (running or not). *)

val evaluate_whenjob : ?onload:bool -> t -> Whenexpr.job -> bool * t
(** This evaluates the whenjob and returns [true] iff the whenjob
    should be run now.

    Note that this returns a possibly-updated state structure. *)
