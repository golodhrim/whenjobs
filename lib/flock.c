/* whenjobs
 * (C) Copyright 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/file.h>

#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <caml/fail.h>

/* flock_exclusive_nonblocking : file_descr -> unit
 *
 * The reason we need this function is that OCaml only gives us access
 * to the 'fcntl'-style of locking.  These locks are not preserved
 * over fork(2) (WTF POSIX?) making them pretty much useless.
 * Therefore use BSD-style flock instead.
 */
value
whenjobs_flock_exclusive_nonblocking (value fdv)
{
  CAMLparam1 (fdv);

  /* file_descr is opaque, but on un*x-like platforms it's an integer */
  int fd = Int_val (fdv);

  if (flock (fd, LOCK_EX|LOCK_NB) == -1)
    caml_failwith (strerror (errno));

  CAMLreturn (Val_unit);
}
