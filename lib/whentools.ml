(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

open Whenexpr

open Big_int
open Printf

let set_variable name value =
  check_valid_variable_name name;
  Whenfile.set_variable name (T_string value)

let set_variable_bool name value =
  check_valid_variable_name name;
  Whenfile.set_variable name (T_bool value)

let set_variable_int name value =
  check_valid_variable_name name;
  Whenfile.set_variable name (T_int (big_int_of_int value))

let set_variable_string = set_variable

let set_variable_float name value =
  check_valid_variable_name name;
  Whenfile.set_variable name (T_float value)

type preinfo = Whenexpr.preinfo

let max n preinfo =
  let name = preinfo.pi_job_name in

  (* Count running jobs with the same name. *)
  let count = List.fold_left (
    fun count ->
      function
      | { pirun_job_name = n } when n = name -> count + 1
      | _ -> count
  ) 0 preinfo.pi_running in

  (* Only let this job run if there are fewer than n already running. *)
  count < n

let one () preinfo = max 1 preinfo

type result = Whenexpr.result

let mailto ?(only_on_failure = false) ?from email result =
  if result.res_code <> 0 || not only_on_failure then (
    let subject =
      sprintf "%s: %s (return code %d)"
        result.res_job_name
        (if result.res_code = 0 then "successful" else "FAILED")
        result.res_code in

    let cmd = sprintf "%s -s %s -a %s"
      Config.mailx
      (Filename.quote subject)
      (Filename.quote result.res_output) in

    let cmd =
      match from with
      | None -> cmd
      | Some from -> sprintf "%s -r %s" cmd from in

    let cmd =
      sprintf "%s %s </dev/null" cmd (Filename.quote email) in

    if Sys.command cmd <> 0 then
      failwith "Whentools.mailto: mailx command failed";
  )
