/* whenjobs
 * (C) Copyright 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#define FACILITY LOG_CRON

/* syslog_notice : string -> unit */
value
whenjobs_syslog_notice (value messagev)
{
  CAMLparam1 (messagev);
  const char *message = String_val (messagev);

  syslog (FACILITY|LOG_NOTICE, "%s", message);

  CAMLreturn (Val_unit);
}

/* syslog_error : string -> unit */
value
whenjobs_syslog_error (value messagev)
{
  CAMLparam1 (messagev);
  const char *message = String_val (messagev);

  syslog (FACILITY|LOG_ERR, "%s", message);

  CAMLreturn (Val_unit);
}
