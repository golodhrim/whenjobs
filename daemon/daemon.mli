(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(** Daemon functions. *)

val init : string -> bool -> unit
  (** Initialize the daemon.  This sets up the socket and should
      be called before the daemon forks.

      The parameters are [jobsdir] and [debug]. *)

val reload_files : unit -> unit
  (** (Re-)load the file(s) [$jobsdir/*.cmo] (in bytecode)
      or [$jobsdir/*.cmxs] (in native code).

      This can raise [Failure] if the operation fails. *)

val main_loop : unit -> unit
  (** Run the main loop. *)
