(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(* Test count to 3, then fire a whenjob. *)

when reloaded () :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=0
>>

every 2 seconds :
<<
  echo $JOBSERIAL $JOBNAME $counter >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

when counter = 3 :
<<
  echo $JOBSERIAL $JOBNAME $counter >\> $HOME/test_output
  whenjobs --daemon-stop
>>
