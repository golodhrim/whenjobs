(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

when reloaded () :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=0
>>

job "counter 1"
every second :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

job "counter 2"
every second :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

job "counter 3"
every second :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

job "counter 4"
every second :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

when increases counter :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
>>

when counter == 10 :
<<
  echo $JOBSERIAL $JOBNAME >\> $HOME/test_output
  whenjobs --daemon-stop
>>
