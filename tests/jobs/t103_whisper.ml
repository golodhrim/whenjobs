(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

when state == "" :
<<
  whenjobs --set state=A
>>

when state == "A" :
<<
  echo state A >\> $HOME/test_output
  sleep 1
  whenjobs --set state=B
>>

when state == "B" :
<<
  echo state B >\> $HOME/test_output
  sleep 1
  # This shouldn't cause state A to run:
  whenjobs --whisper state=A
  sleep 1
  whenjobs --set state=C
>>

when state == "C":
<<
  echo state C >\> $HOME/test_output
  sleep 1
  # This shouldn't cause state B to run:
  whenjobs --whisper state=B
  sleep 1
  whenjobs --set state=D
>>

when state == "D":
<<
  echo state D >\> $HOME/test_output
  sleep 1
  whenjobs --daemon-stop
>>
