#!/bin/bash -
# Copyright (C) 2012 Red Hat Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Run the test script using the real daemon and tools.

unset CDPATH
set -e

testdir=$(pwd)
libdir=$(cd ../../lib; pwd)
toolsdir=$(cd ../../tools; pwd)
daemondir=$(cd ../../daemon; pwd)

export libdir

HOME="$testdir"
export HOME

rm -rf "$testdir/.whenjobs"
mkdir "$testdir/.whenjobs"
rm -f "$testdir/test_output"

PATH="$toolsdir:$daemondir:$PATH"
export PATH

# Kill the daemon and clean up when the script exits.
function cleanup {
    kill `cat "$testdir/.whenjobs/daemon_pid"`
    rm -rf "$testdir/.whenjobs"
}
trap cleanup INT TERM QUIT EXIT

# Run the daemon.
whenjobsd -d

# Copy the job into the directory.
cp "$1" "$testdir/.whenjobs/jobs.ml"

# Upload it to the daemon.
whenjobs --upload --lib "$libdir"

# Now wait until the test completes.  Note we cannot use 'wait'
# because the daemon is not a direct child of the script.
while [ -e /proc/`cat "$testdir/.whenjobs/daemon_pid"` ]; do
    sleep 1
done

trap - INT TERM QUIT EXIT
rm -rf "$testdir/.whenjobs"

# Sort the output by job serial number, so that jobs that happened to
# fire off in parallel are still recorded in order.
sort -n -o "$testdir/test_output" "$testdir/test_output"

# Check the test output matches the expected output.
if ! cmp -s "$testdir/test_output" "$1.expected"; then
    echo "$0: $1: test output did not match expected output"
    echo "test output was:"
    echo "-----"
    cat "$testdir/test_output"
    echo "-----"
    exit 1
fi

rm -f "$testdir/test_output"
