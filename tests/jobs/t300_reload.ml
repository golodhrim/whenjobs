(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(* Test reloading the script. *)

let () =
  Whentools.set_variable_int "counter" 1;
  Whentools.set_variable "state" "A"

every 4 seconds :
<<
  echo $JOBSERIAL $JOBNAME $counter >\> $HOME/test_output
  whenjobs --set --type int counter=$(($counter+1))
>>

when counter == 2 :
<<
  echo $JOBSERIAL $JOBNAME $counter $state >\> $HOME/test_output

  # Actually modify the 'set_variable' lines above in the jobs script.
  mv $HOME/.whenjobs/jobs.ml $HOME/.whenjobs/jobs.ml.old
  sed \
      -e 's/"counter" 1/"counter" 3/' \
      -e 's/"state" "A"/"state" "B"/' \
       < $HOME/.whenjobs/jobs.ml.old > $HOME/.whenjobs/jobs.ml

  whenjobs --upload --lib "$libdir"
>>

when counter == 4 :
<<
  echo $JOBSERIAL $JOBNAME $counter $state >\> $HOME/test_output
  whenjobs --daemon-stop
>>
