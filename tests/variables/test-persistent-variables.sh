# whenjobs
# Copyright (C) 2012 Red Hat Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Test that variables are reloaded across runs.

unset CDPATH
set -e

testdir=$(pwd)
libdir=$(cd ../../lib; pwd)
toolsdir=$(cd ../../tools; pwd)
daemondir=$(cd ../../daemon; pwd)

HOME="$testdir"
export HOME

rm -rf "$testdir/.whenjobs"

PATH="$toolsdir:$daemondir:$PATH"
export PATH

# Kill the daemon and clean up when the script exits.
function cleanup {
    kill `cat "$testdir/.whenjobs/daemon_pid"`
    rm -rf "$testdir/.whenjobs"
}
trap cleanup INT TERM QUIT EXIT

# Run the daemon.
whenjobsd -d

whenjobs --lib "$libdir" --type float --set test_float=4.2
whenjobs --lib "$libdir" --type int --set test_int=42
whenjobs --lib "$libdir" --type string --set test_string=fortytwo

whenjobs --lib "$libdir" --daemon-stop >/dev/null 2>&1 ||:

# Run the daemon again and read back the variables.
whenjobsd -d

output="$(whenjobs --lib "$libdir" --variables | LANG=C sort)"

whenjobs --lib "$libdir" --daemon-stop >/dev/null 2>&1 ||:

trap - INT TERM QUIT EXIT
rm -rf "$testdir/.whenjobs"

if [ "$output" != "JOBSERIAL=0
test_float=4.2
test_int=42
test_string=fortytwo" ]; then
    echo "$0: unexpected variables in output:"
    echo "$output"
    exit 1
fi

rm -rf "$testdir/.whenjobs"
