(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

(* Simple when and every statements.  This is just a test of parsing,
 * not of semantics or execution.
 *)

every second :
<<
  # nothing
>>

every 10 seconds :
<<
  # nothing
>>

every 30 minutes :
<<
  # nothing
>>

when reloaded () :
<<
  # nothing
>>

when changes foo :
<<
  # nothing
>>

when foo = "value" && (bar = "value") :
<<
  # nothing
>>

when 1 = 0 :
<<
  # nothing
>>

when false == true :
<<
  # nothing
>>

when true != true :
<<
  # nothing
>>

when true <> (false || true) :
<<
  # nothing
>>
