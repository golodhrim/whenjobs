(* whenjobs
 * Copyright (C) 2012 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

open Printf

(* Ensures that Whentools module is linked to the program. *)
let _ = Whentools.set_variable

(* This program is passed a single argument, which is the .cmo file to
 * dynamically load.
 *)
let file =
  if Array.length Sys.argv != 2 then (
    eprintf "test_load test.cmo\n";
    exit 1
  );
  Sys.argv.(1)

let () =
  Whenfile.init Whenstate.empty;

  (try
     Dynlink.loadfile file
   with
     Dynlink.Error err ->
       eprintf "test_load: %s: %s\n" file (Dynlink.error_message err);
       exit 1
  );

  let state = Whenfile.get_state () in
  printf "test_load: %s: %d jobs parsed from file\n"
    file (Whenstate.nr_jobs state)
